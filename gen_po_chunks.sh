#!/bin/sh

#ma3lesh quick hack for the moment
#this splits the monolithic pot file into smaller po chunks so multiple translators can work

#some strings may be repeated across files so we need to come up with a plicy for this.


grep '#' drupal.po | grep -v msgid | tr -d '#:0-9;'\
    | tr ' ' '\n'   | sort -u > file_list.txt

for i in `cat file_list.txt`; 
  do 
  msggrep -N $i drupal.po -o $i.po;
done

